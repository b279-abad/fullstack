import { useContext, useState, useEffect } from "react";
import UserContext from "../UserContext";
/*import 'react-minimal-side-navigation/lib/ReactMinimalSideNavigation.css';*/
import {Button, Container, Table} from "react-bootstrap";
import Swal from "sweetalert2";
import Banner from "../components/Banner";
import { Link } from "react-router-dom";
import logo from "../images/logo.png"
import { Navigate } from "react-router-dom"

export default function Dashboard(){
     const { user } = useContext(UserContext);
    /*console.log(user);*/

     // useState
     const [allProducts, setAllProducts] = useState([]);
     const [productId, setProductId] = useState("");
     const [name, setName] = useState("");
     const [description, setDescription] = useState("");
     const [price, setPrice] = useState("0");
     const [stocks, setStocks] = useState("0");
     const [isActive, setIsActive] = useState(false);

     // Get All Products
     const getAllProducts = () => {
      fetch(`http://localhost:4000/products/allProducts`,
         {
         method: "GET",
         headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
         },
      })
      .then(res => res.json())
      .then(data => {
         console.log(data);

         const products = data.map((product) => {
            return (
               <tr key={product._id}>
                  <td>{product._id}</td>
                  <td>{product.name}</td>
                  <td>{product.description}</td>
                  <td>{product.price}</td>
                  <td>{product.stocks}</td>
                  <td>{product.isActive ? "Active" : "Inactive"}</td>
                  <td>
                     {product.isActive ? (
                     <Button variant="danger" onClick={() => archive(product._id)}>Archive</Button>
                     ) : (
                     <>
                     <Button variant="success" onClick={() =>unarchive(product._id)}>Unarchive</Button>

                     <Button as={Link} to={'/products/update'}>Edit</Button>
                     </>
                  )}
               </td>
            </tr>
         );
      });

         setAllProducts(products);
   });
}
      
   useEffect(() => {
      getAllProducts()
   }, [])

   // Archive Product
   const archive = (id) => {
      fetch(`http://localhost:4000/products/archive`,
         {
         method: "PUT",
         headers: {
            "Content-Type" : "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`
         },
         body: JSON.stringify({
            isActive: false,
         }),
      })
      .then(res => res.json())
      .then(data => {
         if(data){
            Swal.fire({
               title: "You have successfully archived this product!",
               icon: "success",
               text: "Product archived.",
            });

            getAllProducts();
         }else{
            Swal.fire({
                title: "Archive Unsuccessful!",
               icon: "error",
               text: "Something went wrong. Please try again.",
            });
         }
      });
   };

    // Unarchive Product
   const unarchive = (id) => {
      fetch(`http://localhost:4000/products/unarchive`, {
         method: "PUT",
         headers: {
            "Content-Type" : "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`
         },
         body: JSON.stringify({
            isActive: false,
         })
      })
      .then(res => res.json())
      .then(data => {
         if(data){
            Swal.fire({
               title: "You have successfully unarchived this product!",
               icon: "success",
               text: "Product now inactive."
            })

            getAllProducts();
         }else{
            Swal.fire({
                title: "Unarchive Unsuccessful!",
               icon: "error",
               text: "Something went wrong. Please try again."
            })
         }
      })
   }

    const dashboardBanner = {
      title: "Welcome to your Dashboard, Admin!",
      content: "Here is where you can control things. Let's work together!",
      buttonDestination: "/products/",
      buttonLabel: "Add New Product",
      image: {logo}
   }
   

   return(
      <>
   {
      localStorage.getItem("isAdmin") === "true" ?
   
      // Admin manage products dashboard table
       <Container>
         <Banner bannerProps={dashboardBanner}/>
         <Table>
            <thead>
               <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Price</th>
                  <th>Stocks</th>
                  <th>Status</th>
                  <th>Actions</th>
               </tr>
            </thead>
            <tbody>
               {allProducts}
            </tbody>
         </Table>
      </Container>
      :

      <Navigate to="/products"/>
   }
   </>
   )
};
