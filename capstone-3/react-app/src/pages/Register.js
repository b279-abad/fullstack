import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { useNavigate,Navigate,Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import {Col, Row} from "react-bootstrap"

export default function Register() {

    const {user} = useContext(UserContext);

    //an object with methods to redirect the user
    const navigate = useNavigate();

    // State hooks to store the values of the input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [isActive, setIsActive] = useState('');

    // Check if values are successfully binded
    console.log(email);
    console.log(password1);
    console.log(password2);

    // Function to simulate user registration
    function registerUser(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            if(data === true){

                Swal.fire({
                    title: 'Duplicate email found',
                    icon: 'error',
                    text: 'Please provide a different email.'   
                });

            } else {

                fetch(`${ process.env.REACT_APP_API_URL }/users/register`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        mobileNo: mobileNo,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {

                    console.log(data);

                    if(data === true){

                        // Clear input fields
                        setFirstName('');
                        setLastName('');
                        setEmail('');
                        setMobileNo('');
                        setPassword1('');
                        setPassword2('');


                        Swal.fire({
                            title: 'Registration successful',
                            icon: 'success',
                            text: 'Welcome to WOOF GANG!'
                        });

                        // Allows us to redirect the user to the login page after registering for an account
                        navigate("/login");

                    } else {

                        Swal.fire({
                            title: 'REGISTRATION NOT SUCCESSFUL',
                            icon: 'error',
                            text: 'Please try again.'   
                        });

                    }

                })
            }

        })

    }

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if((firstName !== '' && lastName !== '' && email !== '' && mobileNo.length === 11 && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [firstName, lastName, email, mobileNo, password1, password2]);

    return (
        (user.token !== null) ?
            <Navigate to="/products" />
        :
        <>
        <Container fluid className="w-100 m-0 p-0">
        <Row className="w-90 m-0 p-0">
         <Col className="banner2-bg m-0 text-light d-flex flex-column align-items-center justify-content-center text-center p-5" xs={12} md={12} lg={6}>
            <h1 className='display-1'>Register for your FURRY FRIENDS!</h1>
            <h1 className='display-5'>From food to toys to grooming supplies, we have everything you need!</h1>
        </Col>
        <Col xs={12} md={12} lg={6} className="d-flex flex-column align-items-center justify-content-center m-0 colr2-bg">
            <div className='w-75 my-5 py-5 shadow-sm shadow-lg rounded bg-white  d-flex flex-column align-items-center justify-content-center'>
            <h1 className=" text-center">REGISTER</h1>

            <Form className="w-75" onSubmit={(e) => registerUser(e)}>

                <Form.Group className="mb-3" controlId="firstName">
                    <Form.Label>First Name</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Enter first name"
                        value={firstName} 
                        onChange={e => setFirstName(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="lastName">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Enter last name"
                        value={lastName} 
                        onChange={e => setLastName(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
    	                type="email" 
    	                placeholder="Enter email"
                        value={email} 
                        onChange={e => setEmail(e.target.value)}
    	                required
                    />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="mobileNo">
                    <Form.Label>Mobile Number</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Enter Mobile Number"
                        value={mobileNo} 
                        onChange={e => setMobileNo(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="password1">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
    	                type="password" 
    	                placeholder="Password"
                        value={password1} 
                        onChange={e => setPassword1(e.target.value)}
    	                required
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="password2">
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control 
    	                type="password" 
    	                placeholder="Verify Password"
                        value={password2} 
                        onChange={e => setPassword2(e.target.value)}
    	                required
                    />
                </Form.Group>

                { isActive ? 
                    <Button className="mt-3" variant="primary" type="submit" id="submitBtn">
                        Register
                    </Button>
                    : 
                    <Button className="mt-3" variant="danger" type="submit" id="submitBtn" disabled>
                        Register
                    </Button>
                }
            <div className='d-flex flex-column align-items-center justify-content-center'>
            <p className='mt-5'>Already have an account? <Link to={"/login"}><strong>Login Here!</strong></Link></p>
            </div>
            </Form>
            </div>
            </Col>
        </Row>
        </Container>
        </>
    )
}