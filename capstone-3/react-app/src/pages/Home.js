import { Navigate } from "react-router-dom";
import { useContext } from "react"
import UserContext from "../UserContext";
import Banner from "../components/Banner";
/*import HotDeals from "../components/HotDeals";
import HomeCarousel from "../components/HomeCarousel";*/
import logo from "../images/logo.png"
/*import FeaturedProducts from "../components/FeaturedProducts";*/


export default function Home(){

	
	const { user } = useContext(UserContext);
	const data = {
		title: "Woof Gang",
		content: "From the newest pet foods to the hottest new toys, you’ll find it all here. This pet shop is all about making your pet’s life as furtastic as possible. From food to toys to grooming supplies, we have everything you need!",
		destination: "/products",
		label: "Shop Now!",
		image: {logo}
	}

	return(
		<>
		{
        (localStorage.getItem("isAdmin") === "true")
		?
		<Navigate to="/dashboard" />
		:
		<div className="p-5">
			{/*<HomeCarousel/>*/}
			<Banner bannerProps={data}/>
			{/*<HotDeals />
			<FeaturedProducts/>*/}
		</div>
		}
		</>
		
	)
}