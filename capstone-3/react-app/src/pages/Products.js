import { useEffect, useState, useContext } from "react";
import { Navigate } from "react-router-dom";
import Banner from "../components/Banner";
import ProductCard from "../components/ProductCard";
import UserContext from "../UserContext";
import logo from "../images/logo.png"
import {Row} from "react-bootstrap"


export default function Products(){

		const data = {
		title: "Woof Gang",
		content: "From the newest pet foods to the hottest new toys, you’ll find it all here. This pet shop is all about making your pet’s life as furtastic as possible. From food to toys to grooming supplies, we have everything you need!",
		destination: "/products/allProducts",
		label: "Shop Now!",
		image: {logo}
	}

	const { user } = useContext(UserContext);


	const [products, setProducts] = useState([]);

	useEffect(() =>{

		fetch(`http://localhost:4000/products/allProducts`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setProducts(data.map(products =>{
				return(
                        <ProductCard key={products._id} productProps={products}/>
				);
			}));
		})
	}, []);


	return(
		(localStorage.getItem("isAdmin") === "true")
		?
			<Navigate to="/dashboard" />
		:	
		<>
        <div className="p-5">
		<Banner bannerProps={data}/>
			<h1>Woof Gang</h1>
			<Row className="my-3 text-dark h-100">
			{products}
			</Row>
        </div>
		</>
	)
}