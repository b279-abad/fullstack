const productsData = [
    {
        id: "item001",
        name: "Pet Carrier Bag",
        description: "Recommended max weight 20lbs.",
        price: 3000,
        onOffer: true
    },
    {
        id: "item002",
        name: "Harness",
        description: "Reflective and adjustable",
        price: 500,
        onOffer: true
    },
    {
        id: "item003",
        name: "Waste Bag",
        description: "Fits standard poop bag.",
        price: 1000,
        onOffer: true
    },
    {
        id: "item004",
        name: "Fence",
        description: "Stackable pet cage. 35x35 cm.",
        price: 3000,
        onOffer: true
    }
]

export default productsData;