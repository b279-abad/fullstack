import { Link, NavLink } from "react-router-dom";
import { useContext } from "react"
import UserContext from "../UserContext"

import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap";

export default function AppNavbar(){

  const { user } = useContext(UserContext);
  const userEmail = `${localStorage.getItem("email")}`
  console.log(user);

  return(
  <Navbar expand="lg" className="shadow bg-white px-3 sticky-top m-0 px-2 text-white">
  <Container fluid>
      <Navbar.Brand as={Link} to={"/"} className="fw-bold">
      <img
            alt=""
            src={require('../images/logo.png')}
            width="30"
            height="30"
            className="d-inline-block align-top rounded"
            />{' '}
      Woof Gang</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ms-auto">
          {
                (localStorage.getItem("isAdmin") === "true")
                ?
                <Nav.Link as={ NavLink } to="/dashboard" end>Dashboard</Nav.Link>
                :
                <>
                <Nav.Link as={ NavLink } to="/" end>Home</Nav.Link>
                <Nav.Link as={ NavLink } to="/products" end>Products</Nav.Link>
                </>
            }
            {
                (user.id !== null)
                ?
                    <>
                    
                    
                    <NavDropdown title={userEmail} align="end" id="collasible-nav-dropdown" end="true">
                    <NavDropdown.Item as={ NavLink } to="/settings" end="true">Account Settings</NavDropdown.Item>
                    <NavDropdown.Item as={ NavLink } to="/report" end="true">Report</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item as={ NavLink } to="/logout" end="true">Logout</NavDropdown.Item>
                    </NavDropdown>
                    </>
                :
                <>
                    <Nav.Link as={ NavLink } to="/login" end="true">Login</Nav.Link>
                    <Nav.Link as={ NavLink } to="/register" end="true">Register</Nav.Link>
                </>
            }
        </Nav>

            </Navbar.Collapse>
        </Container>
        </Navbar>
    )
}