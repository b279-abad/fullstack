import { Link } from "react-router-dom";
import { Row, Col, Button, Image } from "react-bootstrap";


export default function Banner({bannerProps}){
	const {title, content, destination, label, image} = bannerProps;
	return(
        <div className="vh-75 bannerAll-bg text-light d-flex align-items-center justify-content-center mb-5">
		<Row>
			<Col className="text-center d-flex flex-column justify-content-center align-items-center" lg={12}>
                <Image
                className="d-block w-50  carousel-height circle-crop"
                src={require('../images/logobanner.png')}
                roundedCircle
                fluid
                />
				<h2>{title}</h2>
            	<p>{content}</p>
				<Button className="w-50 btn-outline-light" as = {Link} to={'../products/create'} variant="">Create Product</Button>
			</Col>
		</Row>
        </div>
	)
}