import { Card, Button, Col } from 'react-bootstrap';
import {Link} from "react-router-dom";
import Banner from './Banner';

export default function ProductCard({productProps}) {
    // Checks if props was successfully passed
    console.log(productProps.name);
    // checks the type of the passed data
    console.log(typeof productProps);

   // Desturing the courseProp into their own variables
    const { _id, name, description, price, stocks, imgSource } = productProps;
/* 
    // useState
    // Used for storing different states
    // used to keep track of information to individual components

    // SYNTAX -> const [getter, setter] = useState(initialGetterValue);

    const [count, setCount] = useState(0);
    console.log(useState(0));

    const [seat, setSeat] = useState(30);

    // Function that keeps tract of the enrollees for a course
    function enroll(){
        if(seat > 0){
            setCount(count + 1);
            setSeat(seat - 1);
        }else{
            alert("No more seats available!");
        }
        
        console.log("Enrollees: " + count);
        console.log("Available Seats: " + seat);
    }*/

     return (
        <>
        <Col className="my-2" xs={12} md={6} lg={3}>
            <Card className="my-3 card-height  card-border shadow-md card-bg">
            <Card.Img className='img-fluid w-100 product-img-fit'
                src={imgSource}
            />
                <Card.Header className='py-3 mb-3 d-flex align-items-center justify-content-center'>
                <Card.Title>
                    {name}
                </Card.Title>
                </Card.Header>
                <Card.Body>
                <Card.Subtitle>
                    Description:
                </Card.Subtitle>
                <Card.Text>
                    {description}
                </Card.Text>
                <Card.Subtitle>
                    Price:
                </Card.Subtitle>
                <Card.Text>
                    Php {price}
                </Card.Text>
                <Card.Subtitle>
                    Stocks:
                </Card.Subtitle>
                <Card.Text>
                    <p id='stocks'>{stocks} available</p>
                </Card.Text>

            </Card.Body>
            <Card.Footer>
            <Button className='w-100 my-3 btn-outline-dark' variant='none' as={Link} to={`/products/checkout/${_id}`}>CHECKOUT</Button>
            </Card.Footer>
        </Card>
            </Col>
        
        </>
    )
}