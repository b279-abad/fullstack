// DOM Document Object Manipulation

const txtFirstName = document.getElementById("txt-first-name");
const spanFullName = document.getElementById("span-full-name");

// event listener listens or follows to any event / listens to what ever we click in keyboard
txtFirstName.addEventListener("keyup", (event) => {
	// whatever we put in txtFirstName will output in spanFullName
	spanFullName.innerHTML = txtFirstName.value;
})

txtFirstName.addEventListener("keyup", (event) => {
	// whatever we put in txtFirstName will output in spanFullName
	console.log(event.target);
	console.log(event.target.value);
})